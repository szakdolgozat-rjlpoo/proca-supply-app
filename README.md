# Supply App for Proca

[![pipeline status](https://gitlab.com/szakdolgozat-rjlpoo/proca-supply-app/badges/master/pipeline.svg)](https://gitlab.com/szakdolgozat-rjlpoo/proca-supply-app/commits/master)
[![coverage report](https://gitlab.com/szakdolgozat-rjlpoo/proca-supply-app/badges/master/coverage.svg)](https://gitlab.com/szakdolgozat-rjlpoo/proca-supply-app/commits/master)

## Docker

By default Docker file exposes 8130 port.

## Endpoints

### Supplier endpoints

```
GET /suppliers/{supplier-id}
Permit all

Response body:
{
    "id": ""
    "name": ""
}
```

### Stock endpoints

```
GET /suppliers/{supplier-id}/stocks/{product-id}
Permit all

Response body:
{
    "amount": ""
}
```
