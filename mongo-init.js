db.supplier.insertMany([
    {
        "_id": ObjectId("5dae1eb62742da3d8b7163f3"),
        "name": "American supplier",
        "location": "USA",
        "address": {
            "addressLineOne": "123 XY street",
            "city": "New York City",
            "zipCode": "12345",
            "state": "NY",
            "country": "HUN"
        },
        "stocks": [
            {
                "product": 1,
                "amount": 20
            },
            {
                "product": 3,
                "amount": 5
            }
        ]
    },
    {
        "_id": ObjectId("5dae225a8a7d1d18eff0f14e"),
        "name": "Hungarian supplier",
        "address": {
            "addressLineOne": "XY utca 123",
            "city": "Budapest",
            "zipCode": "1234",
            "state": "Budapest",
            "country": "HUN"
        },
        "stocks": [
            {
                "product": 1,
                "amount": 20
            },
            {
                "product": 2,
                "amount": 40
            }
        ]
    }
]);
