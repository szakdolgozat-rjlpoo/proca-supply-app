package xyz.nergal.proca.supply.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupplyAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(SupplyAppApplication.class, args);
    }

}
