package xyz.nergal.proca.supply.app.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.repository.init.Jackson2RepositoryPopulatorFactoryBean;

@Configuration
public class EmbeddedMongoPopulatorConfiguration {

    @Bean
    @ConditionalOnClass(name = {"de.flapdoodle.embed.mongo.MongodStarter"})
    public Jackson2RepositoryPopulatorFactoryBean repositoryPopulator(ResourceLoader resourceLoader) {
        Resource resource = resourceLoader.getResource("classpath:data.json");

        Jackson2RepositoryPopulatorFactoryBean factory = new Jackson2RepositoryPopulatorFactoryBean();
        factory.setResources(new Resource[]{resource});
        return factory;
    }
}
