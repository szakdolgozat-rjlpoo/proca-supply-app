package xyz.nergal.proca.supply.app.controller;

import org.bson.types.ObjectId;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.supply.api.StockResponse;
import xyz.nergal.proca.supply.app.service.StockService;

@RestController
@RequestMapping("/suppliers/{supplier}/stocks")
public class StockController {

    private final StockService stockService;

    public StockController(StockService stockService) {
        this.stockService = stockService;
    }

    @GetMapping(path = "/{product}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<StockResponse>> read(@PathVariable ObjectId supplier, @PathVariable Long product) {
        return stockService.get(supplier, product)
                .onErrorResume(throwable -> Mono.empty())
                .map(supplierResponse -> ResponseEntity.ok().body(supplierResponse))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }
}
