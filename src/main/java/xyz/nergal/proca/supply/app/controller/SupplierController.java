package xyz.nergal.proca.supply.app.controller;

import org.bson.types.ObjectId;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.supply.api.SupplierResponse;
import xyz.nergal.proca.supply.app.service.SupplierService;

@RestController
@RequestMapping("/suppliers")
public class SupplierController {

    private final SupplierService supplierService;

    public SupplierController(SupplierService supplierService) {
        this.supplierService = supplierService;
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<SupplierResponse>> read(@PathVariable ObjectId id) {
        return supplierService.get(id)
                .map(supplierResponse -> ResponseEntity.ok().body(supplierResponse))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }
}
