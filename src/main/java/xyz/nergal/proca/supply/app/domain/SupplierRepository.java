package xyz.nergal.proca.supply.app.domain;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import xyz.nergal.proca.supply.app.domain.document.Supplier;

/**
 * TODO: Replace with {@link ReactiveMongoRepository} when [DATACMNS-1133] is resolved and a fix is published
 * https://jira.spring.io/browse/DATACMNS-1133
 */
@Repository
public interface SupplierRepository extends MongoRepository<Supplier, ObjectId> {
}
