package xyz.nergal.proca.supply.app.domain.document;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Address {

    private String addressLineOne;

    private String addressLineTwo;

    private String city;

    private String zipCode;

    private String state;

    private String country;
}
