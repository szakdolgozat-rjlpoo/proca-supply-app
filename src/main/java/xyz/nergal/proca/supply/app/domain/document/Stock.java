package xyz.nergal.proca.supply.app.domain.document;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Stock {

    private Long product;

    private Long amount;
}
