package xyz.nergal.proca.supply.app.domain.document;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Supplier {

    @MongoId
    private ObjectId id;

    private String name;

    private Address address;

    private List<Stock> stocks;
}
