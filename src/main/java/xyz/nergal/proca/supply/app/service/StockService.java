package xyz.nergal.proca.supply.app.service;

import org.bson.types.ObjectId;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.supply.api.StockResponse;
import xyz.nergal.proca.supply.app.domain.document.Supplier;

public interface StockService {

    /**
     * Return a stock matching the given {@link Supplier} id and product id.
     *
     * When supplier does not exist signal {@link java.util.NoSuchElementException}.
     *
     * When supplier exists but stock does not signal default value.
     *
     * @param supplierId the id of the owner {@link Supplier} entity.
     * @param productId  the id of the product.
     * @return a stock matching the given {@link Supplier} id and product id.
     */
    Mono<StockResponse> get(ObjectId supplierId, Long productId);
}
