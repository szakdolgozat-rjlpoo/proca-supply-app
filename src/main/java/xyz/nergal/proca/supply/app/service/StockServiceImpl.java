package xyz.nergal.proca.supply.app.service;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.supply.api.StockResponse;
import xyz.nergal.proca.supply.app.domain.SupplierRepository;
import xyz.nergal.proca.supply.app.domain.document.Stock;
import xyz.nergal.proca.supply.app.domain.document.Supplier;

import java.util.Objects;
import java.util.Optional;

@Service
public class StockServiceImpl implements StockService {

    private final SupplierRepository supplierRepository;

    public StockServiceImpl(SupplierRepository supplierRepository) {
        this.supplierRepository = supplierRepository;
    }

    @Override
    public Mono<StockResponse> get(ObjectId supplierId, Long productId) {
        return Mono.justOrEmpty(supplierRepository.findById(supplierId))
                .single()
                .flatMapIterable(Supplier::getStocks)
                .filter(stock -> Objects.equals(productId, stock.getProduct()))
                .next()
                .defaultIfEmpty(Stock.builder().product(productId).build())
                .map(stock -> Optional.ofNullable(stock.getAmount()).orElse(0L))
                .map(amount -> StockResponse.builder().amount(amount).build());
    }
}
