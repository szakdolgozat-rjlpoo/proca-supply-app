package xyz.nergal.proca.supply.app.service;

import org.bson.types.ObjectId;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.supply.api.SupplierResponse;

public interface SupplierService {

    /**
     * Return a supplier matching the given id.
     *
     * @param id the id of the expected supplier.
     * @return a supplier matching the given id.
     */
    Mono<SupplierResponse> get(ObjectId id);
}
