package xyz.nergal.proca.supply.app.service;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.supply.api.SupplierResponse;
import xyz.nergal.proca.supply.app.domain.SupplierRepository;

@Service
public class SupplierServiceImpl implements SupplierService {

    private final SupplierRepository supplierRepository;

    public SupplierServiceImpl(SupplierRepository supplierRepository) {
        this.supplierRepository = supplierRepository;
    }

    @Override
    public Mono<SupplierResponse> get(ObjectId id) {
        return Mono.justOrEmpty(supplierRepository.findById(id))
                .map(supplier -> SupplierResponse.builder()
                        .id(supplier.getId().toHexString())
                        .name(supplier.getName())
                        .build());
    }
}
