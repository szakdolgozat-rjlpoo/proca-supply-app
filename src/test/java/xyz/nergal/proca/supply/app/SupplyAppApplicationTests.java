package xyz.nergal.proca.supply.app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SupplyAppApplicationTests {

    @Test
    void contextLoads() {
        // Intentionally Left Blank
        Assertions.assertTrue(true);
    }

}
