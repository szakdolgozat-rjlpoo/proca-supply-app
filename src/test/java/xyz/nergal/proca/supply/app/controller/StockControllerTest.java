package xyz.nergal.proca.supply.app.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.supply.api.StockResponse;
import xyz.nergal.proca.supply.app.config.SecurityConfiguration;
import xyz.nergal.proca.supply.app.service.StockService;

@ExtendWith(SpringExtension.class)
@AutoConfigureWebTestClient
@WebFluxTest({
        SecurityConfiguration.class,
        StockController.class,
})
class StockControllerTest {

    @MockBean
    private StockService stockService;

    @Autowired
    private WebTestClient client;

    @Test
    void readShouldRespondNotFound() {
        Mockito.when(stockService.get(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(Mono.empty());

        this.client.get()
                .uri("/suppliers/aaaaaaaaaaaaaaaaaaaaaaaa/stocks/1")
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void readShouldRespondOk() {
        Mockito.when(stockService.get(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(Mono.just(StockResponse.builder().build()));

        this.client.get()
                .uri("/suppliers/aaaaaaaaaaaaaaaaaaaaaaaa/stocks/1")
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void readShouldRespondWithCorrectPayload() {
        StockResponse stockResponse = StockResponse.builder()
                .amount(1L)
                .build();

        Mockito.when(stockService.get(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(Mono.just(stockResponse));

        this.client.get()
                .uri("/suppliers/aaaaaaaaaaaaaaaaaaaaaaaa/stocks/1")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody().json("{\"amount\":1}");
    }
}
