package xyz.nergal.proca.supply.app.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

@ExtendWith(SpringExtension.class)
@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class SupplierControllerIntegrationTest {

    @Autowired
    private WebTestClient client;

    @Test
    void readShouldRespondNotFoundWhenSupplierDoesNotExist() {
        this.client.get()
                .uri("/suppliers/aaaaaaaaaaaaaaaaaaaaaaaa")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void readShouldRespondOkWhenSupplierExists() {
        this.client.get()
                .uri("/suppliers/5dae1eb62742da3d8b7163f3")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk();
    }
}
