package xyz.nergal.proca.supply.app.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.supply.api.SupplierResponse;
import xyz.nergal.proca.supply.app.config.SecurityConfiguration;
import xyz.nergal.proca.supply.app.service.SupplierService;

@ExtendWith(SpringExtension.class)
@AutoConfigureWebTestClient
@WebFluxTest({
        SecurityConfiguration.class,
        SupplierController.class,
})
class SupplierControllerTest {

    @MockBean
    private SupplierService supplierService;

    @Autowired
    private WebTestClient client;

    @Test
    void readShouldRespondNotFound() {
        Mockito.when(supplierService.get(ArgumentMatchers.any()))
                .thenReturn(Mono.empty());

        this.client.get()
                .uri("/suppliers/aaaaaaaaaaaaaaaaaaaaaaaa")
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void readShouldRespondOk() {
        Mockito.when(supplierService.get(ArgumentMatchers.any()))
                .thenReturn(Mono.just(SupplierResponse.builder().build()));

        this.client.get()
                .uri("/suppliers/aaaaaaaaaaaaaaaaaaaaaaaa")
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void readShouldRespondWithCorrectPayload() {
        SupplierResponse supplierResponse = SupplierResponse.builder()
                .id("object-id")
                .name("name")
                .build();

        Mockito.when(supplierService.get(ArgumentMatchers.any()))
                .thenReturn(Mono.just(supplierResponse));

        this.client.get()
                .uri("/suppliers/aaaaaaaaaaaaaaaaaaaaaaaa")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody().json("{\"id\":\"object-id\",\"name\":\"name\"}");
    }
}
