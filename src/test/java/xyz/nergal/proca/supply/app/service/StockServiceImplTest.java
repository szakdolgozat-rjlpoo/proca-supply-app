package xyz.nergal.proca.supply.app.service;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.test.StepVerifier;
import xyz.nergal.proca.supply.api.StockResponse;
import xyz.nergal.proca.supply.app.domain.SupplierRepository;
import xyz.nergal.proca.supply.app.domain.document.Stock;
import xyz.nergal.proca.supply.app.domain.document.Supplier;

import java.util.Collections;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@Import({StockServiceImpl.class})
class StockServiceImplTest {

    @MockBean
    private SupplierRepository supplierRepository;

    @Autowired
    private StockService stockService;

    @Test
    void getShouldReturnErrorWhenSupplierDoesNotExist() {
        ObjectId supplierId = new ObjectId("5dae1eb62742da3d8b7163f3");
        long productId = 1L;

        Mockito.when(supplierRepository.findById(ArgumentMatchers.eq(supplierId)))
                .thenReturn(Optional.empty());

        StepVerifier.create(stockService.get(supplierId, productId))
                .verifyError();
    }

    @Test
    void getShouldReturnDefaultWhenStockDoesNotExist() {
        ObjectId supplierId = new ObjectId("5dae1eb62742da3d8b7163f3");
        long productId = 1L;

        Supplier supplier = Supplier.builder()
                .stocks(Collections.emptyList())
                .build();

        Mockito.when(supplierRepository.findById(ArgumentMatchers.eq(supplierId)))
                .thenReturn(Optional.of(supplier));

        StepVerifier.create(stockService.get(supplierId, productId))
                .expectNext(StockResponse.builder().amount(0L).build())
                .verifyComplete();
    }

    @Test
    void getShouldHandleNullAmount() {
        ObjectId supplierId = new ObjectId("5dae1eb62742da3d8b7163f3");
        long productId = 1L;

        Stock stock = Stock.builder()
                .product(productId)
                .amount(null)
                .build();

        Supplier supplier = Supplier.builder()
                .stocks(Collections.singletonList(stock))
                .build();

        Mockito.when(supplierRepository.findById(ArgumentMatchers.eq(supplierId)))
                .thenReturn(Optional.of(supplier));

        StepVerifier.create(stockService.get(supplierId, productId))
                .expectNext(StockResponse.builder().amount(0L).build())
                .verifyComplete();
    }

    @Test
    void get() {
        ObjectId supplierId = new ObjectId("5dae1eb62742da3d8b7163f3");
        long productId = 1L;

        Stock stock = Stock.builder()
                .product(productId)
                .amount(1L)
                .build();

        Supplier supplier = Supplier.builder()
                .stocks(Collections.singletonList(stock))
                .build();

        Mockito.when(supplierRepository.findById(ArgumentMatchers.eq(supplierId)))
                .thenReturn(Optional.of(supplier));

        StepVerifier.create(stockService.get(supplierId, productId))
                .expectNext(StockResponse.builder().amount(1L).build())
                .verifyComplete();
    }
}
