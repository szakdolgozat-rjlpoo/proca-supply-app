package xyz.nergal.proca.supply.app.service;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.test.StepVerifier;
import xyz.nergal.proca.supply.api.SupplierResponse;
import xyz.nergal.proca.supply.app.domain.SupplierRepository;
import xyz.nergal.proca.supply.app.domain.document.Supplier;

import java.util.Optional;

@ExtendWith(SpringExtension.class)
@Import({SupplierServiceImpl.class})
class SupplierServiceImplTest {

    @MockBean
    private SupplierRepository supplierRepository;

    @Autowired
    private SupplierService supplierService;

    @Test
    void getShouldReturnEmptyWhenSupplierDoesNotExist() {
        ObjectId id = new ObjectId("5dae2304ebc826b8d2e34d26");

        Mockito.when(supplierRepository.findById(ArgumentMatchers.eq(id)))
                .thenReturn(Optional.empty());

        StepVerifier.create(supplierService.get(id))
                .verifyComplete();
    }

    @Test
    void get() {
        ObjectId id = new ObjectId("5dae2304ebc826b8d2e34d26");

        Mockito.when(supplierRepository.findById(ArgumentMatchers.eq(id)))
                .thenReturn(Optional.of(Supplier.builder().id(id).build()));

        StepVerifier.create(supplierService.get(id))
                .expectNext(SupplierResponse.builder().id("5dae2304ebc826b8d2e34d26").build())
                .verifyComplete();
    }
}
